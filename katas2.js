function add(x, y) {
    z = x + y;
    return z
}

function multiply(x, y) {
    let z = 0;
    if (x > 0) {
        for (i =x; i > 0; i--) {
            z = z + y;
        }
    }
    else if (x < 0) {
        for (i = x; i < 0; i++) {
            z = z - y;
        }
    }
    else if (x===0) {
        z = 0;
    }
    return z;
}

function power(x, y) {
    k = x;
    for (i = y; y > 1; y--) {
        k = k * x;
    }
    return k;
};

function factorial(x) {
    var n = 1
    var v = 1
    while (x != 0) {
        n = multiply(n, v);
        v++;
        x--;
    };
    return n;
};

function fibonacci(n) {
    var a = 0;
    var b = 1;
    var x;
    if (n === 0) {
        return 0;
    }
    else if(n === 1) {
        return 1
    }
    else if (n > 0) {
        for (i = 0; i < (n-2); i++) {
            x = a + b;
            a = b;
            b = x;
        }
    }
    return x;
};